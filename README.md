# WebLogic Prometheus Exporter - WLPE

This project is a J2EE web application that exposes WebLogic Server metrics to Prometheus.

## Build
Use Maven to build the web application artefact wlpe.war

## Installation
Deploy the wlpe.war application in your WebLogic domain with WLST, the admin console or weblogic.Deployer.

Authentication is required to access the wlpe application. The user must have Monitor or Administrator role to be able to acess the web app.

## Prometheus Configuration
Scrap config sample : 

scrape_configs:
  - job_name: 'weblogic'

    metrics_path: '/wlpe/metrics'
 
    static_configs:
    - targets: ['localhost:7001']
    
    basic_auth:
      username: 'weblogic'
      password: 'welcome1'


## Run
Landing page : /wlpe
Metrics page : /wlpe/metrics

