/**
 * 
 */
package fr.corsaireconsulting.wlpe;

import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.management.Attribute;
import javax.management.AttributeList;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import fr.corsaireconsulting.wlpe.ConfigManager.MBean;



/**
 * @author Emmanuel
 *
 */
final class MetricCollector implements IConfigurationChangeListener {

	private static final java.util.logging.Logger LOGGER = weblogic.logging.LoggingHelper.getServerLogger().getLogger("WLPE");
	
	private MBeanServer mbs = null;
	private String domainName = null;
	private String serverName = null;

	private ConfigManager cm = null;

	private List<MBeanToMonitor> mbeansToMonitor = null;
	
	// Date et heure en ms de la derni�re collecte de donn�es
	private long lastCollectionTime=-1L;

	private static class SingletonHelper{
		private static final MetricCollector INSTANCE = new MetricCollector();
	}

	public static MetricCollector getInstance() {
		return SingletonHelper.INSTANCE;
	}

	/**
	 * 
	 */
	private MetricCollector() {
		LOGGER.info("Initializing MetricCollector");


		mbeansToMonitor = new ArrayList<MBeanToMonitor>(0);
		mbs = ManagementFactory.getPlatformMBeanServer();

		try {
			javax.management.ObjectName service = new javax.management.ObjectName("com.bea:Name=RuntimeService,Type=weblogic.management.mbeanservers.runtime.RuntimeServiceMBean");
			javax.management.ObjectName dconf = (javax.management.ObjectName)mbs.getAttribute(service, "DomainConfiguration");


			javax.management.ObjectName serverConf = (javax.management.ObjectName)mbs.getAttribute(service, "ServerRuntime");

			domainName = (String)(mbs.getAttribute(dconf, "Name"));
			serverName = (String)(mbs.getAttribute(serverConf, "Name"));

			//			jvmRuntime = new ObjectName("com.bea:ServerRuntime="+serverName+",Name="+serverName+",Type=JRockitRuntime");
			//
			LOGGER.fine("domaineName:"+domainName);
			LOGGER.fine("serverName:"+serverName);
			
			cm = ConfigManager.getInstance();
			cm.addListener(this);
			
			configurationChange();

		} catch (MalformedObjectNameException e) {
		} catch (NullPointerException e) {
		} catch (AttributeNotFoundException e) {
		} catch (InstanceNotFoundException e) {
		} catch (MBeanException e) {
		} catch (ReflectionException e) {
		}
	}

	/**
	 * @return the domainName
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * @return the serverName
	 */
	public String getServerName() {
		return serverName;
	}

	final static class MBeanToMonitor {

		private ObjectName objectName;
		private List<String> attributes;
		private List<String> key;
		private String type;
		
		MBeanToMonitor(String type, ObjectName on) {
			super();
			this.objectName = on;
			attributes = new ArrayList<String>(1);
			key = new ArrayList<String>(1);
			this.type = type;
		}

		void addAttribute(String attributeName) {
			attributes.add(attributeName);
		}

		void addKey(String keyName) {
			key.add(keyName);
		}

		List<String> getAttributes() {
			return attributes;
		}

		List<String> getKey() {
			return key;
		}

		public ObjectName getObjectName() {
			return objectName;
		}

		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}


	}

	@Override
	public void configurationChange() {
		LOGGER.info("Loading configuration");
		
		// La recherche des mbeans se fait en 2 temps � cause de la clef Type qui peut �tre �crite "Type" ou "type"
		for (MBean mbean : cm.getMBeans()) {
			LOGGER.fine("Looking for mbean "+mbean.getType());
			String objectNameStr = String.format(mbean.getDomain()+":Type=%s,*", mbean.getType());
			
			try {
				ObjectName scope = new ObjectName(objectNameStr);
				Set<ObjectName> objectNames = mbs.queryNames(scope, null);
				
				
				if (null == objectNames || 0 == objectNames.size()) {

					objectNameStr = String.format(mbean.getDomain()+":type=%s,*", mbean.getType());
					scope = new ObjectName(objectNameStr);
					objectNames = mbs.queryNames(scope, null);
					if (null == objectNames || 0 == objectNames.size()) {
						LOGGER.severe("Unable to find MBean '"+mbean.getDomain()+" "+mbean.getType()+"'.");
						continue;
					}
				}
				
				
				for (ObjectName on : objectNames) {
					boolean mbeanShouldBeMonitored = false;

					// Filtrage sur le mbean ?
					Map<String, String> filter = mbean.getFilter();
					if (null != filter && 0 < filter.keySet().size()) {
						LOGGER.fine("MBean type is filtered");
						for (String property : filter.keySet()) {
							String filteredValue = filter.get(property);
							
							LOGGER.fine("Property '"+property+"': "+on.getKeyProperty(property));
							if (filteredValue.equals(on.getKeyProperty(property))) {
								mbeanShouldBeMonitored = true;
								break;
							}
						}
					} else mbeanShouldBeMonitored = true;
					
					if (mbeanShouldBeMonitored) {
						MBeanToMonitor mtm = new MBeanToMonitor(mbean.getType(), on);
	
						// V�rification des attributs constituant la clef
						for (String attr : mbean.getKey()) {
							try {
								mbs.getAttribute(on, attr);
								mtm.addKey(attr);
							} catch(Exception e) {
								LOGGER.severe("Unable to get value for attribute (key) '"+attr+"', the MBean will be exceluded from monitoring.");
								continue;
							}
						}
						
						LOGGER.fine("MBean "+on.toString()+" will be monitored");
	
						// V�rification et chargement de la liste des attributs � r�cup�rer
						for (String attr : mbean.getAttributes()) {
							try {
								mbs.getAttribute(on, attr);
								mtm.addAttribute(attr);
							} catch(Exception e) {
								LOGGER.severe("Unable to get value for attribute '"+attr+"' it will be exceluded from monitoring.");
							}
						}
						mbeansToMonitor.add(mtm);
					} else {
						LOGGER.fine("MBean "+on.toString()+" will NOT be monitored");
						// if
					}
				}				
			} catch (MalformedObjectNameException e1) {
				LOGGER.severe("Unable to get mbean '"+objectNameStr+"', it will be exceluded from monitoring.");
			}


		}

	}
	
	public void collect(PrintWriter out) {
		LOGGER.fine("Collecting metrics");
		if (null == mbeansToMonitor || 0 == mbeansToMonitor.size()) return;
		long start = System.currentTimeMillis();
		
		// Nb de valeurs lues
		int scrapCount = 0;
		int mbeanCount = 0;
		for (MBeanToMonitor mbean : this.mbeansToMonitor) {
			LOGGER.fine("Getting values for mbean "+mbean.getObjectName().toString());
			String[] attrNames = (String[]) mbean.getAttributes().toArray(new String[mbean.getAttributes().size()]);
			String[] key = (String[]) mbean.getKey().toArray(new String[mbean.getKey().size()]);
			try {
				String keyStr = "";
				AttributeList keyValues = mbs.getAttributes(mbean.getObjectName(), key);
				for (int i=0; i<keyValues.size(); i++) {
					Attribute at = (Attribute)keyValues.get(i);
					LOGGER.fine(at.getName()+":"+at.getValue());
					if (keyStr.length() > 0) keyStr+=",";
					keyStr += at.getName()+"=\""+at.getValue()+"\"";
				}

				
				AttributeList values = mbs.getAttributes(mbean.getObjectName(), attrNames);
				for (int i=0; i<values.size(); i++) {
					Attribute at = (Attribute)values.get(i);
					LOGGER.fine(at.getName()+":"+at.getValue());
					
					out.print("wlpe_"+mbean.getType()+"_"+at.getName()+"{domain=\""+domainName+"\",server=\""+serverName+"\","+keyStr+"} "+at.getValue()+"\n");
				}
				
				scrapCount += values.size();
				mbeanCount++;
			} catch (InstanceNotFoundException | ReflectionException e) {
				LOGGER.severe("An error occured while getting values, "+e);
			}
		}
		
		long elapsed = System.currentTimeMillis()-start;
		double elapsed_SECONDS=(elapsed / 1000.0d);
		out.print("wlpe_scrape_mbeans_count_total{domain=\""+domainName+"\",server=\""+serverName+"\"} "+String.valueOf(mbeanCount)+"\n");
		out.print("wlpe_scrape_duration_seconds{domain=\""+domainName+"\",server=\""+serverName+"\"} "+String.valueOf(elapsed_SECONDS)+"\n");
		LOGGER.fine("Collect done for "+scrapCount+" values in "+elapsed+" ms");
		
		lastCollectionTime = System.currentTimeMillis();
	}

	/**
	 * @return the lastCollectionTime
	 */
	public long getLastCollectionTime() {
		return lastCollectionTime;
	}


}
