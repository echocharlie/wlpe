package fr.corsaireconsulting.wlpe;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.management.ManagementFactory;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class MainServlet
 * GET : 
 * /metrics
 * /MBeanInfos
 * /reloadConf
 * /showConf
 * /
 * /help
 * 
 * POST :
 * 
 * Logging Infos : This component uses WebLogic/JDK logging services. All logs are sent to WebLogic logging services.
 * The logging domain is "WLPE"
 * See https://docs.oracle.com/middleware/12212/wls/WLLOG/config_logs.htm#WLLOG162
 * To setup logging level in WebLogic : 
 *   - WLST
 *   	use the set command to set the value of the LoggerSeverityProperties attribute of the LogMBean
 *   - Console
 *     -Servers/[server]/Logging/Advances
 *       update property "Niveaux de journaliseur de la plate-forme:": 
 *       WLPE=Trace
 *   - System Property :
 *     -Dweblogic.Log.LoggerSeverityProperties="WLPE=Trace"
 */
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private final static String HTML_HEADER = "<HTML><BODY>";
	private final static String HTML_FOOTER = "</BODY></HTML>";

	private final static java.util.logging.Logger LOGGER = weblogic.logging.LoggingHelper.getServerLogger().getLogger("WLPE");

	private MetricCollector mc = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public MainServlet() {
		super();
	}

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		LOGGER.info("Initializing WLPE main servlet.");
		mc = MetricCollector.getInstance();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//		response.getWriter().append("Served at: ").append(request.getContextPath());

		
		
		if ("/help".equals(request.getServletPath())) {
			request.getRequestDispatcher("/index.jsp").forward(request, response);
			
			return;
		}
		
		if ("/metrics".equals(request.getServletPath())) {
			response.setContentType("text/plain");
			PrintWriter out = response.getWriter();
			mc.collect(out);
			return;
		}

		if ("/showConf".equals(request.getServletPath())) {
//			System.out.println("\n\npath:"+request.getContextPath());
//			System.out.println("\nuri:"+request.getRequestURI());
//			System.out.println("servletPath:"+request.getServletPath());
			StringBuilder buf = new StringBuilder();

			buf.append(HTML_HEADER);

			buf.append("<PRE>");
			buf.append(ConfigManager.getInstance().getConf());
			buf.append("</PRE>");

			buf.append(HTML_FOOTER);
			response.getWriter().append(buf.toString());
			return;
		}
		
		if ("/reloadConf".equals(request.getServletPath())) {

			StringBuilder buf = new StringBuilder();
			buf.append(HTML_HEADER);

			try {
				ConfigManager.getInstance().reloadConf();
				buf.append("<PRE>");
				buf.append(ConfigManager.getInstance().getConf());
				buf.append("</PRE>");
			} catch (TechnicalException e) {
				e.printStackTrace();
			}

			buf.append(HTML_FOOTER);
			response.getWriter().append(buf.toString());		
			return;
		}
		
		if ("/mbeans".equals(request.getServletPath())) {
			StringBuilder buf = new StringBuilder();
			buf.append(HTML_HEADER);
			buf.append("<UL>");
			try {
				MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
				ObjectName on = new ObjectName("*.*:*");
				Set<ObjectName> mbeans = mbs.queryNames(on, null);
				for (ObjectName n : mbeans) {
//					System.out.println("MBean : "+n);
					buf.append("<li>").append(n).append("</li>");
				}
			} catch (MalformedObjectNameException e) {
			} catch (NullPointerException e) {
			}

			buf.append("</UL");
			buf.append(HTML_FOOTER);
			response.getWriter().append(buf.toString());		
			return;
		}
		
		request.getRequestDispatcher("/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
