/**
 * 
 */
package fr.corsaireconsulting.wlpe;

/**
 * @author Emmanuel
 *
 */
public class TechnicalException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7422693763313585292L;

	/**
	 * @param arg0
	 */
	public TechnicalException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public TechnicalException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public TechnicalException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
