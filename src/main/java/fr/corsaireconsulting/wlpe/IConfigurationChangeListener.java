/**
 * 
 */
package fr.corsaireconsulting.wlpe;

/**
 * @author Emmanuel
 *
 */
public interface IConfigurationChangeListener {
	
	public void configurationChange();

}
