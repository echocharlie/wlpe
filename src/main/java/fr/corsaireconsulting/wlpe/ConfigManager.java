/**
 * 
 */
package fr.corsaireconsulting.wlpe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

/**
 * @author Emmanuel
 *
 */
final class ConfigManager {
	
	private static final String CONFIG_FILE = "/resources/wlpe.conf";
	private static final java.util.logging.Logger LOGGER = weblogic.logging.LoggingHelper.getServerLogger().getLogger("WLPE");

	private MBean[] mbeans = null;
	
	// Liste des abbon�s au changement de configuration
	private List<IConfigurationChangeListener> listeners = null;
	
	public MBean[] getMBeans() {
		return this.mbeans;
	}
	
	// Ajout d'un abonn�
	public void addListener(IConfigurationChangeListener listener) {
		if (! listeners.contains(listener)) listeners.add(listener);
	}
	
	// Rechargement de la conf et notification aux abonn�s
	public void reloadConf() throws TechnicalException{
		loadConf();
		
		for (IConfigurationChangeListener listener : listeners) {
			listener.configurationChange();
		}
	}
	
	
	private static class SingletonHelper{
		private static final ConfigManager INSTANCE = new ConfigManager();
	}

	public static ConfigManager getInstance() {
		return SingletonHelper.INSTANCE;
	}
	
	/**
	 * @return configuration file content
	 */
	public String getConf() {
		StringBuilder buf = new StringBuilder();
		
		InputStream is = null;

		try {
			is = ConfigManager.class.getResourceAsStream(CONFIG_FILE);
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			while ((line = reader.readLine()) != null) {
				buf.append(line);
				buf.append("\n");
			}
		} catch (IOException e) {
//			e.printStackTrace();
		} finally {
			if (null != is) {
				try {
					is.close();
				} catch (IOException e) {
				}
			}
		}
		return buf.toString();
	}
	
	private void loadConf() throws TechnicalException {
		LOGGER.info("Loading configuration");
		InputStream is = null;
		try { 
			is = ConfigManager.class.getResourceAsStream(CONFIG_FILE);
			ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
			YamlConfigFile configFile = mapper.readValue(is, YamlConfigFile.class);

			this.mbeans = configFile.getMbeans();
			for(MBean aMBean : mbeans) {
				LOGGER.info(aMBean.getType());
				
				if (null != aMBean.getAttributes()) {
					for (String attr : aMBean.getAttributes()) {
						LOGGER.info(attr);
					}
				}
			}
		} catch (JsonParseException e) {
			throw new TechnicalException("Unable to load configuration from '"+CONFIG_FILE+"', exception :"+e);
		} catch (JsonMappingException e) {
			throw new TechnicalException("Unable to load configuration from '"+CONFIG_FILE+"', exception :"+e);
		} catch (java.io.IOException e) {
			throw new TechnicalException("Unable to load configuration from '"+CONFIG_FILE+"', exception :"+e);
		} finally {
			if (null != is) {
				try {
					is.close();
				} catch (IOException e) {
				}
				
			}
		}
	}

	/**
	 * 
	 */
	public ConfigManager() {
		super();
		listeners = new ArrayList<IConfigurationChangeListener>(0);
		
		try {
			loadConf();
		} catch(TechnicalException e) {
			LOGGER.severe("Unable to load configuration, "+e.getMessage());
		}
		

	}

	
	final static class YamlConfigFile  {
		
		private MBean[] mbeans;
		

		/**
		 * @return the mbeans
		 */
		public MBean[] getMbeans() {
			return mbeans;
		}

		/**
		 * @param mbeans the mbeans to set
		 */
		private void setMbeans(MBean[] mbeans) {
			this.mbeans = mbeans;
		}

		
		
	}
	
	final static class MBean {
		private String type;
		private String domain;
		
		private Map<String, String> filter;
		
		// La clef permet d'identifier une instance de MBean
		// Elle peut �tre constitu�e de plusieurs segments, par exemple PARTITION, NAME
		private String[] key;
		
		private String[] attributes;

		/**
		 * @return the type
		 */
		public String getType() {
			return type;
		}

		/**
		 * @param type the type to set
		 */
		private void setType(String type) {
			this.type = type;
		}

		/**
		 * @return the attributes
		 */
		public String[] getAttributes() {
			return attributes;
		}

		/**
		 * @param attributes the attributes to set
		 */
		private void setAttributes(String[] attributes) {
			this.attributes = attributes;
		}

		/**
		 * @return the key
		 */
		public String[] getKey() {
			return key;
		}

		/**
		 * @param key the key to set
		 */
		private void setKey(String[] key) {
			this.key = key;
		}

		/**
		 * @return the domain
		 */
		public String getDomain() {
			return domain;
		}

		/**
		 * @param domain the domain to set
		 */
		private void setDomain(String domain) {
			this.domain = domain;
		}

		/**
		 * @return the filter
		 */
		public Map<String, String> getFilter() {
			return filter;
		}

		/**
		 * @param filter the filter to set
		 */
		private void setFilter(Map<String, String> filter) {
			this.filter = filter;
		}
	}
	
	
	public static void main(String[] args) {
		
		ConfigManager cm = ConfigManager.getInstance();
		
	}

}
